package com.shopping1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.shopping1.model.Cart;

public class CartDao {

	//private ResultSet rs;


	public CartDao() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public List<Cart> getCart() throws ClassNotFoundException, SQLException {
		Class.forName("org.mariadb.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
		
		PreparedStatement pt = con.prepareStatement("select * from cart");
		ResultSet st = pt.executeQuery();
		List<Cart> ls = new ArrayList<>();

		while (st.next()) {
			int quantity = st.getInt(1);
			int id = st.getInt(2);
			String username = st.getString(3);
			ls.add(new Cart(quantity, id, username));
		}
		return ls;
	}

	public int add_cart(Cart c) throws ClassNotFoundException {
		int record = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");

			
			PreparedStatement ps = con.prepareStatement("insert into cart values(?,?,?)");
			ps.setInt(2, c.getQuantity());
			ps.setInt(1, c.getId());
			ps.setString(3, c.getUsername());
			record = ps.executeUpdate();
			return record;

		} catch (Exception e) {
			e.printStackTrace();

		}
		return record;

	}

	public int deleteCart(int id) throws ClassNotFoundException, SQLException {
		Class.forName("org.mariadb.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
		
		PreparedStatement ps = con.prepareStatement("delete from cart where id=?");
		ps.setInt(1, id);
		int res = ps.executeUpdate();
		return res;
	}

}
