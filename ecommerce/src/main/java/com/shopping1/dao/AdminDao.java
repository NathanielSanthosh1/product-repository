package com.shopping1.dao;

import java.sql.*;

import com.shopping1.model.Admin;

public class AdminDao {

	private String query;
	private PreparedStatement pst;
	private ResultSet rs;
	private Connection con;
	

	

	public AdminDao() {
	}

	

	public Admin adminLogin(String email, String password) throws ClassNotFoundException {
		Admin admin = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			query = "select * from admin where email=? and password=?";
			pst = con.prepareStatement(query);
			pst.setString(1, email);
			pst.setString(2, password);
			rs = pst.executeQuery();
			if (rs.next()) {
				admin = new Admin();
				admin.setName(rs.getString("name"));
				admin.setEmail(rs.getString("email"));
			}
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
		return admin;
	}
	public int signup(Admin ad) throws ClassNotFoundException {
		int record = 0;
		try {
//			Class.forName("org.mariadb.jdbc.Driver");
//			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			
			PreparedStatement ps = this.con.prepareStatement("insert into admin values(?,?,?)");
			//ps.setInt(1,ad.getId());
			ps.setString(1, ad.getName());
			ps.setString(2, ad.getEmail());
			ps.setString(3, ad.getPassword());
			record = ps.executeUpdate();
			return record;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return record;
	}
}