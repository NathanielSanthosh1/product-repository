package com.shopping1.dao;

import java.sql.*;

import com.shopping1.model.User;

public class UserDao {

	private String query;
	private PreparedStatement pst;
	private ResultSet rs;

	public UserDao() {
	}

	public User userLogin(String email, String password) throws ClassNotFoundException {
		User user = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			query = "select * from users where email=? and password=?";
			pst = con.prepareStatement(query);
			pst.setString(1, email);
			pst.setString(2, password);
			rs = pst.executeQuery();
			if (rs.next()) {
				user = new User(rs.getString("email"),rs.getString("password"));
				//user.setName(rs.getString("email"));
				//user.setEmail(rs.getString("password"));
				return user;
			}
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
		return user;
	}
	public int signup(User us) throws ClassNotFoundException {
		int record = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			
			PreparedStatement ps =con.prepareStatement("insert into users values(?,?,?)");
			ps.setString(1, us.getName());
			ps.setString(2, us.getEmail());
			ps.setString(3, us.getPassword());
			record = ps.executeUpdate();
			return record;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return record;
	}
}