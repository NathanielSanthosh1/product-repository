package com.shopping1.dao;

import java.sql.*;
import java.util.*;

import com.shopping1.model.Cart;
import com.shopping1.model.Product;

public class ProductDao {
	
	private String query;
	private PreparedStatement pst;
	private ResultSet rs;


	
	public ProductDao() {
	}

	
	public List<Product> getAllProducts() throws ClassNotFoundException {
		List<Product> book = new ArrayList<>();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			query="select * from products";
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				Product row = new Product(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDouble(4));
				//System.out.println(rs.getInt(1));
				

				book.add(row);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return book;
	}

	public List<Product> getSingleProduct(int id) throws ClassNotFoundException {
		Product prod = new Product();
		List<Product> ls=new ArrayList<>()	;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			
			PreparedStatement ps = con.prepareStatement("select * from product where id=?");
			ps.setInt(1,id);
			ResultSet rs =ps.executeQuery();
			while(rs.next()) {
				prod=new Product(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDouble(4),rs.getString(5));
				ls.add(prod);
			}
			
			return ls;
		} 
		catch (SQLException e) 
		{
			
			e.printStackTrace();
		}
		
		return ls;
	}


	
	public int addProduct(Product pr) throws ClassNotFoundException {
		int record = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			
			PreparedStatement ps = con.prepareStatement("insert into products values(?,?,?,?,?)");
			ps.setInt(1,pr.getId());
			ps.setString(2, pr.getName());
			ps.setString(3, pr.getCategory());
			ps.setDouble(4, pr.getPrice());
			ps.setString(5, pr.getUsername());
			record = ps.executeUpdate();
			return record;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return record;
	}

	public int deleteProduct(Product pr) throws ClassNotFoundException {
		int record=0;
		try
		{
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce_cart","root","root");
			
			PreparedStatement ps = con.prepareStatement("delete from products where id=? and name=? ");
			ps.setInt(1, pr.getId());
			ps.setString(2, pr.getName());
			record = ps.executeUpdate();
			return record;
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return record;
		
	}
		
	}
	
	
	
	

