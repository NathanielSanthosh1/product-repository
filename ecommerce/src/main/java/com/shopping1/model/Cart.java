package com.shopping1.model;

public class Cart {
	private int quantity;
	private int id;
	private String username;
	

	public Cart(int id, String username) {
		super();
		this.id = id;
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Cart( int id,int quantity, String username) {
		super();
		this.quantity = quantity;
		this.id = id;
		this.username = username;
	}

	public Cart() {
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}