package com.shopping1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.shopping1.dao.CartDao;
import com.shopping1.model.Cart;

@WebServlet(name = "AddToCartServlet", urlPatterns = "/add-to-cart")
public class AddToCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Cart> ls = new ArrayList<>();
		try {

			CartDao cd = new CartDao();
			PrintWriter pw = resp.getWriter();
			ls = cd.getCart();
			Gson gs = new Gson();
			String json = gs.toJson(ls);
			resp.setContentType("application/json");
			pw.write(json);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// response.setContentType("text/html;charset=UTF-8");

		try (PrintWriter out = response.getWriter()) {

			// ArrayList<Cart> cartList = new ArrayList<>();
			int id = Integer.parseInt(request.getParameter("id"));
			int quantity = Integer.parseInt(request.getParameter("quantity"));
			String username = request.getParameter("name");

			Cart cm = new Cart(id, quantity, username);
			CartDao cart = new CartDao();
			int i = 0;
//			
			try {
				i = cart.add_cart(cm);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String status = "";
			if (i > 0) {
				status = "product added to cart successfully";
			} else {
				status = "product is already in the cart";
			}
			Gson gs = new Gson();
			String json = gs.toJson(status);
			out.write(json);

		}
	}

	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int pid = Integer.parseInt(req.getParameter("id"));
		CartDao dao = new CartDao();
		String s = "";

		try {
			int res = dao.deleteCart(pid);
			if (res > 0) {
				s = "Deleted";
			} else {
				s = "Not";
			}
			Gson gs = new Gson();
			PrintWriter pw = resp.getWriter();
			pw.write(gs.toJson(s));
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
}
