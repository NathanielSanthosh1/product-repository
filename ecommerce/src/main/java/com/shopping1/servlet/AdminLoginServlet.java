package com.shopping1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shopping1.dao.AdminDao;
import com.shopping1.model.Admin;


@WebServlet("/AdminLoginServlet")
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().append("Served at: ").append(req.getContextPath());

		}
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			String email = request.getParameter("login-email");
			String password = request.getParameter("login-password");
			AdminDao adao = new AdminDao();
			Admin admin = adao.adminLogin(email, password);
			if (admin != null) {
				//request.getSession().setAttribute("author", admin);
				//response.sendRedirect("Productupdate.jsp");
				out.println("admin log in successful");
				
			} else {
				out.println("there is no admin");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
