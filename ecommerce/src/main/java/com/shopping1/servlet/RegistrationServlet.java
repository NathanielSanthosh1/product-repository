package com.shopping1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.protocol.x.SyncFlushDeflaterOutputStream;
import com.shopping1.dao.UserDao;
import com.shopping1.model.User;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().append("Served at: ").append(req.getContextPath());

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String name = request.getParameter("username");
		//System.out.println(name);
		String email = request.getParameter("login-email");
		String password = request.getParameter("login-password");
		try {
			UserDao udao = new UserDao();
			User us = new User(name, email, password);
			int u = udao.signup(us);
			if (u>0) {
				//out.println("you're  registration done successfully.");
				//response.sendRedirect("login.jsp");
				out.write("successfully Registered");
			}
			else
			{
				out.write("you are already registered");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

}
