package com.shopping1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.shopping1.dao.ProductDao;
import com.shopping1.model.Product;

/**
 * Servlet implementation class AddProductServlet
 */
@WebServlet("/AddProductServlet")
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public AddProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 int prod_id=Integer.parseInt(request.getParameter("id"));
		 String prod_name=request.getParameter("name");
		 String prod_cat=request.getParameter("category");
		 double prod_price=Double.parseDouble(request.getParameter("price"));
		 String username = request.getParameter("username");
		 PrintWriter out = response.getWriter();
		 try {
		 Product pr=new Product(prod_id, prod_name, prod_cat, prod_price,username);
		 ProductDao pda=new ProductDao();
		 int i=0;
		i = pda.addProduct(pr);
		 String status="";
		 if(i>0)
		 {
			 status="product added successfully";
		 }
		 else
		 {
			 status="product is already there";
		 }
		 Gson gs=new Gson();
		 String json=gs.toJson(status);
		 out.write(json);
		 }
		 catch (Exception e) {
				e.printStackTrace();
		 
		 
	}

}
}
