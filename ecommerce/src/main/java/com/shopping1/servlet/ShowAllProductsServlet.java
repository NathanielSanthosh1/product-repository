package com.shopping1.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.google.gson.Gson;

import com.shopping1.dao.ProductDao;
import com.shopping1.model.Product;

@WebServlet("/ShowAllProductsServlet")
public class ShowAllProductsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException { 
		try {
		List<Product> list=new ArrayList<Product>();
	           ProductDao dao=new ProductDao();
	           list=dao.getAllProducts();
	           
		
		     Gson gs=new Gson();
	         response.setContentType("application/json");
	         PrintWriter out=response.getWriter();
	         String json=gs.toJson(list);
	        out.write(json);
	         } 
	        catch (Exception e) {
		       e.printStackTrace();
		       }
	 		}
		
	


}


