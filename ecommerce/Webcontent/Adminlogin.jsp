<%@page import="com.shopping1.model.*"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
Admin auth = (Admin) request.getSession().getAttribute("author");
if (auth != null) {
	//response.sendRedirect("index.jsp");
}


%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/includes/head.jsp"%>
<%@include file="/includes/navbar.jsp"%>
<title>E-Commerce Cart</title>
</head>
<body>
	<div class="container">
		<div class="card w-50 mx-auto my-5">
			<div class="card-header text-center">Admin Login</div>
			<div class="card-body">
				<form action="AdminLoginServlet" method="post">
					<div class="form-group">
						<label>Email address</label> <input type="email"
							name="login-email" class="form-control" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label>Password</label> <input type="password"
							name="login-password" class="form-control" placeholder="Password">
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-primary">Login</button>
					</div>
				</form>
				<a href="AdminRegister.jsp"> Don't have account? Signup </a>
			</div>
		</div>
	</div>
	<%@include file="/includes/foot.jsp"%>

</body>
</html>