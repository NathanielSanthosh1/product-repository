
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Product</title>
</head>
<%@include file="/includes/head.jsp"%>
<body>
	<h1>Add Product</h1>
	<form action="AddProductServlet" method="post">
		<label for="id">Product ID:</label> <input type="number" id="id"
			name="id"><br>
		<br> <label for="name">Product Name:</label> <input type="text"
			id="name" name="name"><br>
		<br> <label for="category">Product Category:</label> <input
			type="text" id="category" name="category"><br>
		<br> <label for="price">Product Price:</label> <input
			type="number" id="price" name="price"><br>
		<br> <input type="submit" value="Add Product">
	</form>
	<%@include file="/includes/foot.jsp"%>

</body>
</html>