<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New Admin </title>
<%@include file="includes/head.jsp"%>
</head>
<body>
<div class="container">
		<div class="card w-50 mx-auto my-5">
			<div class="card-header text-center">Admin Register</div>
			<div class="card-body">
				<form action="AdminRegisterServlet" method="post">
					<div class="form-group">
						<label>Admin name :</label> <input type="text"
							class="form-control" name="name"
							placeholder="Enter username">
						<label>Email Address :</label> <input type="email"
							class="form-control" name="login-email" placeholder="Enter email">
						<label>Password :</label> <input type="password"
							class="form-control" name="login-password" placeholder="********">
					</div>
					<div class="text-center">
						<button type="submit" class="btn-btn-primary">Register</button>
					</div>
					<br>
				</form>
			</div>
		</div>
	</div>

	<%@include file="includes/foot.jsp"%>


</body>
</html>