
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Delete Product</title>
</head>
<%@include file="/includes/head.jsp"%>
<body>
	<h1>Delete Product</h1>
	<form action="DeleteProductServlet" method="post">
		<label for="id">Product ID:</label> <input type="number" id="id"
			name="id"><br>
		<br> <label for="name">Product Name:</label> <input type="text"
			id="name" name="name"><br>
		<br> <input type="submit" value="Delete Product">
	</form>
	<%@include file="/includes/foot.jsp"%>

</body>
</html>
